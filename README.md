# Backend-End Development Test

Esta prueba sirve para poder medir las habilidades del candidato en el uso de django
junto con django rest-framework para la creación ágil de APIs REST.  

## La tarea

El reto se va a dividir en tres partes, dos de las cuales son obligatorias y una ultima
opcional:

* Parte A: Creación de un modelo de datos en Django.
* Parte B: Creación de los endpoints.
* Parte C: documentación de los endpoints usando django swagger (opcional)


### Parte A

Se debe implementar en django una aplicación que contenga el siguiente modelo de datos:

```json
{
  "inmueble": {
    "tipo": "string",
    "subtipo": "string",
    "general": {
      "direccion": "string",
      "ciudad": "string",
      "departamento": "string",
      "pais": "string",
      "telefono": "string"
    },
    "interior": {
      "cuartos": "float",
      "baños": "integer",
      "closets": "integer",
      "calentador": "boolean"
    },
    "exterior": {
      "vigilancia": "string",
      "parqueadero": "string",
      "salon_social": "boolean",
      "numero_pisos": "integer"
    }
  }
}

```

Dicho modelo de datos debe ser usado en una base de datos postgres, y debe contar con validaciones
mínimas como el tipo de datos. Todos los valores son requeridos a excepción de los booleanos.

### Parte B

Se debe realizar la creación de los serializadores y de los views para poder hacer acceso
a los endpoints. Se debe garantizar que un usuario no autenticado pueda realizar las siguientes
operaciones:

* POST /inmueble para la creación de un nuevo inmueble con todas las características.
* PUT /inmueble/{id} para la actualización de un inmueble existente.
* DELETE /inmuebles/{id} para la eliminación de un inmueble existente.
* GET /inmueble/{id} para imprimir en pantalla el json de un inmueble almacenado.
* GET /inmuebles para imprimir el listado total de inmuebles.

### Parte C (opcional)

Se debe crear un endpoint:

* GET /api/doc para usuarios no autenticados, es decir, publico, en donde se pueda observar
la documentación en swagger de los endpoints implementados

## Instrucciones adicionales

* Realizar un fork al repositorio actual
* Las dependencias de django, django-rest-framework y django-swagger ya estan instaladas
* Tras finalizar, enviarnos el vinculo a su repositorio.
* Implementar un README es obligatorio
* Crear el dockerfile para probar la aplicación.


## Puntos adicionales

* Tests
* Código limpio
* Código comentado
* Git hooks.
* Hacer uso de pipenv
* Hacer uso del archivo .env
